namespace Connections {
    public class SecretSchemas {
        private static SecretSchemas instance = null;
        public Secret.Schema thief_secret;

        public void load_secret (string alias, string endpoint, string type) {
            var attributes = new GLib.HashTable<string,string> (str_hash, str_equal);
            attributes["type"] = type;
            attributes["endpoint"] = endpoint;
            attributes["alias"] = alias;

            Secret.password_lookupv.begin (thief_secret, attributes, null, (obj, async_res) => {
                try {
                    string? the_secret = Secret.password_lookup.end (async_res);
                    if (the_secret != null) {
                        print ("Secret %s loaded\n", alias);
                    }
                    Secret.password_wipe (the_secret);
                } catch (Error e) {
                    print ("Error loading from keyring: %s\n", e.message);
                }
            });
        }

        public void save_secret (string alias, string url, string type, string secret) {
            var attributes = new GLib.HashTable<string,string> (str_hash, str_equal);
            attributes["type"] = type;
            attributes["endpoint"] = url;
            attributes["alias"] = alias;

            print ("Attempting to save secret %s\n", alias);
            Secret.password_storev.begin (
                thief_secret,
                attributes,
                Secret.COLLECTION_DEFAULT,
                "%s:%s".printf(url, alias),
                secret,
                null, (obj, async_res) =>
            {
                bool res = false;
                try {
                    res = Secret.password_store.end (async_res);
                } catch (Error e) {
                    print ("Error with libsecret: %s", e.message);
                }
                if (res) {
                    print ("Secret %s saved successfully\n", alias);
                } else {
                    print ("Secret %s not saved, but entered callback\n", alias);
                }
            });
        }

        public SecretSchemas () {
            thief_secret = new Secret.Schema (
                "com.kmwallio.valaconnections", Secret.SchemaFlags.NONE,
                "type", Secret.SchemaAttributeType.STRING,
                "endpoint", Secret.SchemaAttributeType.STRING,
                "alias", Secret.SchemaAttributeType.STRING);
        }

        public static SecretSchemas get_instance () {
            if (instance == null) {
                instance = new SecretSchemas ();
            }

            return instance;
        }
    }

    public class NotAPasswordManager : Gtk.Application {
        public NotAPasswordManager () {
            Object (
                application_id: "com.github.kmwallio.valaconnections",
                flags: ApplicationFlags.FLAGS_NONE
            );
        }

        Gtk.ApplicationWindow main_window;

        private void retrieve_password () {
            Gtk.Grid grid = new Gtk.Grid ();
            grid.margin = 12;
            grid.row_spacing = 12;
            grid.column_spacing = 12;
            grid.orientation = Gtk.Orientation.VERTICAL;
            grid.hexpand = true;
            grid.vexpand = true;

            Gtk.Label username_label = new Gtk.Label (_("Username"));
            username_label.xalign = 0;
            Gtk.Entry username_entry = new Gtk.Entry ();
            username_entry.text = "random-username";

            Gtk.Label endpoint_label = new Gtk.Label (_("Endpoint"));
            endpoint_label.xalign = 0;
            Gtk.Entry endpoint_entry = new Gtk.Entry ();
            endpoint_entry.text = "random-string";

            Gtk.Label type_label = new Gtk.Label (_("Type"));
            type_label.xalign = 0;
            Gtk.Entry type_entry = new Gtk.Entry ();
            type_entry.text = "random-type";

            grid.attach (username_label, 1, 1, 1, 1);
            grid.attach (username_entry, 2, 1, 2, 1);
            grid.attach (endpoint_label, 1, 3, 1, 1);
            grid.attach (endpoint_entry, 2, 3, 2, 1);
            grid.attach (type_label, 1, 4, 1, 1);
            grid.attach (type_entry, 2, 4, 2, 1);

            grid.show_all ();

            var dialog = new Gtk.Dialog.with_buttons (
                            "Get Password",
                            main_window,
                            Gtk.DialogFlags.MODAL,
                            _("_Get Password"),
                            Gtk.ResponseType.ACCEPT,
                            _("_Cancel"),
                            Gtk.ResponseType.REJECT,
                            null);

            dialog.get_content_area ().add (grid);

            dialog.response.connect ((response_id) => {
                if (response_id == Gtk.ResponseType.ACCEPT) {
                    SecretSchemas.get_instance ().load_secret (username_entry.text, endpoint_entry.text, type_entry.text);
                }
                dialog.destroy ();
            });
            dialog.show_all ();
        }

        private void store_password () {
            Gtk.Grid grid = new Gtk.Grid ();
            grid.margin = 12;
            grid.row_spacing = 12;
            grid.column_spacing = 12;
            grid.orientation = Gtk.Orientation.VERTICAL;
            grid.hexpand = true;
            grid.vexpand = true;

            Gtk.Label username_label = new Gtk.Label (_("Username"));
            username_label.xalign = 0;
            Gtk.Entry username_entry = new Gtk.Entry ();
            username_entry.text = "random-username";

            Gtk.Label password_label = new Gtk.Label (_("Password"));
            password_label.xalign = 0;
            Gtk.Entry password_entry = new Gtk.Entry ();
            password_entry.text = "random-password";

            Gtk.Label endpoint_label = new Gtk.Label (_("Endpoint"));
            endpoint_label.xalign = 0;
            Gtk.Entry endpoint_entry = new Gtk.Entry ();
            endpoint_entry.text = "random-string";

            Gtk.Label type_label = new Gtk.Label (_("Type"));
            type_label.xalign = 0;
            Gtk.Entry type_entry = new Gtk.Entry ();
            type_entry.text = "random-type";

            grid.attach (username_label, 1, 1, 1, 1);
            grid.attach (username_entry, 2, 1, 2, 1);
            grid.attach (password_label, 1, 2, 1, 1);
            grid.attach (password_entry, 2, 2, 2, 1);
            grid.attach (endpoint_label, 1, 3, 1, 1);
            grid.attach (endpoint_entry, 2, 3, 2, 1);
            grid.attach (type_label, 1, 4, 1, 1);
            grid.attach (type_entry, 2, 4, 2, 1);

            grid.show_all ();

            var dialog = new Gtk.Dialog.with_buttons (
                            "New Password",
                            main_window,
                            Gtk.DialogFlags.MODAL,
                            _("_Add Account"),
                            Gtk.ResponseType.ACCEPT,
                            _("_Cancel"),
                            Gtk.ResponseType.REJECT,
                            null);

            dialog.get_content_area ().add (grid);

            dialog.response.connect ((response_id) => {
                if (response_id == Gtk.ResponseType.ACCEPT) {
                    SecretSchemas.get_instance ().save_secret (username_entry.text, endpoint_entry.text, type_entry.text, password_entry.text);
                }
                dialog.destroy ();
            });
            dialog.show_all ();
        }

        protected override void activate () {
            main_window = new Gtk.ApplicationWindow (this);
            Gtk.Button add_password = new Gtk.Button.with_label ("Add Password");
            add_password.clicked.connect (() => {
                store_password ();
            });
            Gtk.Button get_password = new Gtk.Button.with_label ("Get Password");
            get_password.clicked.connect (() => {
                retrieve_password ();
            });
            Gtk.Grid grid = new Gtk.Grid ();
            grid.margin = 12;
            grid.row_spacing = 12;
            grid.column_spacing = 12;
            grid.orientation = Gtk.Orientation.VERTICAL;
            grid.hexpand = true;
            grid.vexpand = true;

            grid.add (add_password);
            grid.add (get_password);

            main_window.add (grid);
            main_window.show_all ();
        }

        public static int main (string[] args) {
            var app = new NotAPasswordManager ();
            return app.run (args);
        }
    }
}